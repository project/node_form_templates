INTRODUCTION
------------

Create templates which will prefill fields when creating a new node.

On the node edit form a dropdown element will be displayed on top of the form
allowing the user to choose a template.

Templates are stored in YAML format.

REQUIREMENTS
------------

No requirements.

RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Manage Node Form Templates

     Required to manage the Node Form Templates.

 * Customize the Node Form Templates settings in Administration » Settings »
   Content » Node Form Templates Settings.
