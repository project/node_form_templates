<?php

namespace Drupal\node_form_templates\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Provides the settings form for this module.
 */
class NFTSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'node_form_templates_settings_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $example_yaml = <<<EOD
content-type-machine-name:
  template-machine-name:
    label: Human readable template name
    default: false
    fields:
      field_name: Field value
      multi_value_field:
        - Value 1
        - Value 2
      paragraphs_field:
        - {type: paragraph_type, field_name_1: Value, field_name_2: Value}
        - {type: paragraph_type, field_name_1: Value, field_name_2: Value}
EOD;

    // Create description for the YAML settings field.
    $yaml_description = $this->t('Node Form Templates in YAML format. Example: <pre>@example</pre>', ['@example' => $example_yaml]);

    // Create description for token support.
    $token_description = $this->t('Tokens are supported if the token module is enabled.<br />Wrap the token in double quotes when used in a paragraphs field. Example: <pre>{type: paragraph_type, field_name: "[some:token]"}</pre>');

    // Node templates in JSON.
    $form['node_form_templates_yaml'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Node Form Templates YAML'),
      '#description' => $yaml_description . '<br />' . $token_description,
      '#default_value' => $this->config('node_form_templates.settings')->get('node_form_templates_yaml'),
      '#rows' => 20,
    ];

    // Group submit handlers in an actions element with a key of "actions".
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validate the JSON.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Do a YAML check on the tk_node_template_templates field.
    if (!empty($form_state->getValue('node_form_templates_yaml'))) {
      try {
        Yaml::parse($form_state->getValue('node_form_templates_yaml'));
      }
      catch (ParseException $exception) {
        $form_state->setErrorByName('node_form_templates_yaml', $this->t('Invalid YAML in Node Templates.'));
      }
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the JSON.
    $this->configFactory()
      ->getEditable('node_form_templates.settings')
      ->set('node_form_templates_yaml', $form_state->getValue('node_form_templates_yaml'))
      ->save();

    // Show message.
    $this->messenger()->addMessage($this->t('Settings saved.'));
  }

}
